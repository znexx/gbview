ARCH ?= x86_64
CROSS_COMPILE ?= linux-gnu

SHELL=/bin/sh
BUILD_DIR=build

CC:=gcc
CFLAGS:=-std=c99 -pedantic -Wall -Wextra -O3
LDFLAGS:=-L. -lSDL2 -lm -pedantic -Wall -Wextra

ifneq ($(CROSS_COMPILE),linux-gnu)
	CFLAGS+= -ISDL2-2.0.12/$(ARCH)-$(CROSS_COMPILE)/include/
	LDFLAGS=-LSDL2-2.0.12/$(ARCH)-$(CROSS_COMPILE)/lib/ $(shell SDL2-2.0.12/$(ARCH)-$(CROSS_COMPILE)/bin/sdl2-config --static-libs) -lm
endif

target_name = linux
ifeq ($(CROSS_COMPILE), w64-mingw32)
	target_name = win
endif

target_bits = 64
ifeq ($(ARCH), i686)
	target_bits = 32
endif

SRC=$(wildcard *.c)
BIN=$(BUILD_DIR)/gbview-$(target_name)$(target_bits)/gbview
ifeq ($(target_name), win)
	BIN:=$(BIN).exe
endif

INSTALLDIR=/usr/bin

.PHONY: clean $(TESTFILE)

all: $(BIN)

$(BIN): $(SRC)
	mkdir -p $(dir $@)
	$(ARCH)-$(CROSS_COMPILE)-$(CC) -o $@ $(SRC) $(CFLAGS) $(LDFLAGS)
ifeq ($(target_name), win)
	cp SDL2-2.0.12/$(ARCH)-$(CROSS_COMPILE)/bin/SDL2.dll $(dir $@)
endif

run: $(BIN)
	$(BIN)

install: $(BIN)
	mv $(BIN) $(INSTALLDIR)/

uninstall:
	rm $(INSTALLDIR)/$(notdir $(BIN))

test: $(BIN) $(TESTFILE)
	$(BIN) test.2bpp
	$(BIN) -w9 -s8 -v -i test.2bpp -m test.tilemap

clean:
	rm -rf $(BUILD_DIR)
