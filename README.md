# GBVIEW

`gbview` is a viewer for Gameboy formatted images

![Screenshot](/screenshot.png)

## Requirements
The SDL2 development library is the only requisite for compiling

GNU Make is a confirmed working make implementation.
The makefile uses gcc by default, but I trust that if you are using another C-compiler, you can change that yourself.

## Features
- `gbview` uses the default palette of [BGB](http://bgb.bircd.org/).
- The palette can be mirrored (or inverted if you will) with the `-i` option or by pressing 'i' on your keyboard.
- `gbview` can use a separate tilemap file for when the tiles in the input file are e.g. unordered or repeated.
- An integer factor of scaling can be applied for easier viewing of fine details. This scaling can be adjusted while viewing the image.
- Left clicking anywhere in the image prints which tile coordinate and the number of the tile under your cursor. This can be useful when dealing with tilemaps.
- You can either specify the width or height in tiles, or none of them, and `gbview` will guess an as square format as possible.
- Drag and drop support; you can drop files onto the `gbview` window to open them. This does not support tilemaps.
- A grid is available for easier distinction between different tiles.
- Manually adjusting the width and height of the window can be done with the arrow keys.

## Download
Pre-built binaries for linux, and 32 and 64 bit windows can be found on the [downloads page](https://bitbucket.org/znexx/gbview/downloads/).

## Command line usage
Usage: gbview [options] (filename)

| Options           | Description              |
| ----------------- | ------------------------ |
| -i                | Invert palette           |
| -m (tilemap)      | Use tilemap              |
| -s (scale factor) | Use scaling factor       |
| -v                | Verbose                  |
| -w (width)        | Width of image in tiles  |
| -h (height)       | Height of image in tiles |

## Hotkeys

| Hotkey    | Description                                |
| --------- | ------------------------------------------ |
| Escape    | Quits the program                          |
| g         | Turns the grid on or off                   |
| i         | Inverts the palette                        |
| + (Plus)  | Increases the scaling                      |
| - (Minus) | Decreases the scaling                      |
| Spacebar  | Prints info about the image                |
| Up        | Decreases the number of tile rows shown    |
| Down      | Increases the number of tile rows shown    |
| Left      | Decreases the number of tile columns shown |
| Right     | Increases the number of tile columns shown |

## Compiling
You compile `gbview` by issuing the command `make` in the source folder. To cross compile for Windows under linux, supply the variable CROSS\_COMPILE like so: `make CROSS_COMPILE=w64-mingw32`

To cross compile in Linux for 32-bit Windows, run `make CROSS_COMPILE=w64-mingw32 ARCH=i686`.

## Installation
To install `gbview` in Linux, simply compile gbview with `make`, and the install it system-wide by running `sudo make install`. If you want to install `gbview` only for your user, change `INSTALLDIR` to the appropriate dir, e.g. `~/bin`.

## Uninstalling
Uninstalling is as easy as running `sudo make uninstall`.

## Testing
No automatic testing is performed. There is one test case however, which is run with `make test` under Linux.
