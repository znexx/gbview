#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>

#define DEFAULT_SCALING 8
#define TILE_WIDTH 8
#define TILE_HEIGHT 8
#define BPP 2
#define TILE_SIZE (TILE_HEIGHT*BPP)
#define BUFFER_SIZE (256*256*TILE_SIZE)
#define TILEMAP_SIZE (20*18*TILE_SIZE)

SDL_Window *window;
SDL_Renderer *renderer;
SDL_Texture *spritesheet;
char *arg0;
int running = 1;
int redraw = 1;
int verbose = 0;
int using_tilemap = 0;
char tile_data[BUFFER_SIZE];
char tile_map[TILEMAP_SIZE];
char *in_filename;
char *tilemap_filename;
int width_tiles;
int height_tiles;
int tiles_read;
int scale_factor;
int draw_grid;

SDL_Color palette[] = {
	{.r = 224, .g = 248, .b = 208},
	{.r = 136, .g = 192, .b = 112},
	{.r =  52, .g = 104, .b =  86},
	{.r =   8, .g =  24, .b =  32}
};

SDL_Color grid_color = {
	.r = 224,
	.g = 24,
	.b = 128
};

void invert_palette(void) {
	SDL_Color temp_color;
	temp_color = palette[3];
	palette[3] = palette[0];
	palette[0] = temp_color;
	temp_color = palette[2];
	palette[2] = palette[1];
	palette[1] = temp_color;
}

void update_window(void) {
	char window_title[256];

	if (tilemap_filename != NULL) {
		snprintf(
			window_title,
			256,
			"%s (tilemap: %s) (%dx%d tiles = %dx%d px)",
			in_filename,
			tilemap_filename,
			width_tiles,
			height_tiles,
			width_tiles * TILE_WIDTH,
			height_tiles * TILE_HEIGHT
		);
	} else {
		snprintf(
			window_title,
			256,
			"%s (%dx%d tiles = %dx%d px)",
			in_filename,
			width_tiles,
			height_tiles,
			width_tiles * TILE_WIDTH,
			height_tiles * TILE_HEIGHT
		);
	}

	SDL_SetWindowTitle(window, window_title);

	SDL_SetWindowSize(
		window,
		width_tiles * TILE_WIDTH * scale_factor,
		height_tiles * TILE_HEIGHT * scale_factor
	);
}

void keyDown(SDL_Keycode code) {
	switch (code) {
		case SDLK_ESCAPE:
			running = 0;
			break;
		case SDLK_g:
			draw_grid ^= 1;
			if (verbose) {
				printf("Turning grid %s.\n", draw_grid ? "on" : "off");
			}
			redraw = 1;
			break;
		case SDLK_i:
			if (verbose) {
				printf("Inverting palette.\n");
			}
			invert_palette();
			redraw = 1;
			break;
		case SDLK_UP:
			if (height_tiles - 1 >= 1) {
				height_tiles--;
				update_window();
				redraw = 1;
			}
			break;
		case SDLK_DOWN:
			height_tiles++;
			update_window();
			redraw = 1;
			break;
		case SDLK_RIGHT:
			width_tiles++;
			update_window();
			redraw = 1;
			break;
		case SDLK_LEFT:
			if (width_tiles - 1 >= 1) {
				width_tiles--;
				update_window();
				redraw = 1;
			}
			break;
		case SDLK_PLUS:
			if (verbose) {
				printf("Increasing scaling.\n");
			}
			scale_factor++;
			update_window();
			redraw = 1;
			break;
		case SDLK_MINUS:
			if (verbose) {
				printf("Increasing scaling.\n");
			}
			if (scale_factor - 1 >= 1) {
				scale_factor--;
				update_window();
				redraw = 1;
			}
			break;
		case SDLK_SPACE:
			printf("Input filename: %s\n", in_filename);
			printf("Tilemap filename: %s\n", using_tilemap ? tilemap_filename : "(not using tilemap)");
			printf("Image size in tiles: %dx%d\n", width_tiles, height_tiles);
			printf("Image size in pixels: %dx%d\n", TILE_WIDTH * width_tiles, TILE_HEIGHT * height_tiles);
			printf("Image size on screen: %dx%d\n", TILE_WIDTH * width_tiles * scale_factor, TILE_HEIGHT * height_tiles * scale_factor);
			printf("Scale factor: %d\n", scale_factor);
			break;
		default:
			break;
	}
}

void keyUp(SDL_Keycode code) {
	switch (code) {
		default:
			break;
	}
}

void usage(void) {
	printf("Usage: %s [options] (filename)\n", arg0);
	printf("Options:\n");
	printf(" -i                Invert palette\n");
	printf(" -m (tilemap)      Use tilemap\n");
	printf(" -s (scale factor) Use scaling factor\n");
	printf(" -v                Verbose\n");
	printf(" -w (width)        Width of image in tiles\n");
	printf(" -h (height)       Height of image in tiles\n");
	exit(-1);
}

void redraw_image(void) {

	update_window();

	SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
	SDL_RenderClear(renderer);

	if (using_tilemap) {
		char tile_no;
		char byte1;
		char byte2;
		int value;
		SDL_Rect rect = {.w = scale_factor, .h = scale_factor};
		for (int row = 0; row < height_tiles; row++) {
			for (int col = 0; col < width_tiles; col++) {
				tile_no = tile_map[width_tiles * row + col];
				for (int y = 0; y < TILE_HEIGHT; y++) {
					byte1 = tile_data[tile_no * TILE_SIZE + y * BPP];
					byte2 = tile_data[tile_no * TILE_SIZE + y * BPP + 1];
					rect.y = scale_factor * (row * TILE_HEIGHT + y);
					for (int x = 0; x < TILE_WIDTH; x++) {
						value = (((byte2 >> x) & 0x01) << 1) | ((byte1 >> x) & 0x01);
						SDL_SetRenderDrawColor(renderer, palette[value].r, palette[value].g, palette[value].b, SDL_ALPHA_OPAQUE);
						rect.x = scale_factor * (col * TILE_WIDTH + (TILE_WIDTH - (x + 1)));
						SDL_RenderFillRect(renderer, &rect);
					}
				}
			}
		}
	} else {
		// render an image without tilemap
		char byte1;
		char byte2;
		int value;
		SDL_Rect rect = {.w = scale_factor, .h = scale_factor};
		for (int row = 0; row < height_tiles; row++) {
			for (int col = 0; col < width_tiles; col++) {
				for (int y = 0; y < TILE_HEIGHT; y++) {
					byte1 = tile_data[row * width_tiles * TILE_SIZE + col * TILE_SIZE + y * BPP];
					byte2 = tile_data[row * width_tiles * TILE_SIZE + col * TILE_SIZE + y * BPP + 1];
					rect.y = scale_factor * (row * TILE_HEIGHT + y);
					for (int x = 0; x < TILE_WIDTH; x++) {
						value = (((byte2 >> x) & 0x01) << 1) | ((byte1 >> x) & 0x01);
						SDL_SetRenderDrawColor(renderer, palette[value].r, palette[value].g, palette[value].b, SDL_ALPHA_OPAQUE);
						rect.x = scale_factor * (col * TILE_WIDTH + (TILE_WIDTH - (x + 1)));
						SDL_RenderFillRect(renderer, &rect);
					}
				}
			}
		}
	}

	if (draw_grid) {
		SDL_SetRenderDrawColor(renderer, grid_color.r, grid_color.g, grid_color.b, SDL_ALPHA_OPAQUE);
		SDL_Rect grid_rect = {0, 0, TILE_WIDTH * scale_factor, TILE_HEIGHT * scale_factor};
		for (int row = 0; row < height_tiles; row++) {
			grid_rect.y = row * TILE_HEIGHT * scale_factor;
			for (int col = 0; col < width_tiles; col++) {
				grid_rect.x = col * TILE_WIDTH * scale_factor;
				SDL_RenderDrawRect(renderer, &grid_rect);
			}
		}
	}
}

int read_tiles(FILE *in_file, FILE *tilemap_file) {
	int byte;
	int buffer_ptr = 0;
	do {
		byte = fgetc(in_file);
		tile_data[buffer_ptr++] = byte;
	} while (byte != EOF);

	if ((buffer_ptr - 1) % TILE_SIZE) {
		printf("Input file does not contain a whole number of tiles!\n");
		return -1;
	}

	tiles_read = (buffer_ptr - 1) / TILE_SIZE;

	if (tilemap_file) {
		buffer_ptr = 0;

		do {
			byte = fgetc(tilemap_file);
			tile_map[buffer_ptr++] = byte;
		} while (byte != EOF);

		tiles_read = buffer_ptr - 1;
	}
	return tiles_read;
}

int open_file(void) {
	FILE *in_file = NULL;
	FILE *tilemap_file = NULL;

	if (verbose) {
		printf("Opening file %s", in_filename);
		if (tilemap_filename) {
			printf(" with tilemap %s", tilemap_filename);
		}
	}

	if (in_filename) {
		in_file = fopen(in_filename, "r");
		if (in_file == NULL) {
			printf("Could not open file %s.\n", in_filename);
			return 1;
		}
	} else {
		printf("No input file specified!\n");
		return 1;
	}

	if (tilemap_filename != NULL) {
		tilemap_file = fopen(tilemap_filename, "r");
		if (tilemap_file == NULL) {
			printf("Could not open tilemap %s.\n", tilemap_filename);
			return 1;
		}
	}

	read_tiles(in_file, tilemap_file);

	if (tiles_read == -1) {
		return 1;
	}

	if (verbose) {
		printf("\n%d tiles read.\n", tiles_read); //newline is prepended to this row because if we put newline at the end of 278-283 we get double newlines?
	}

	if (width_tiles >= 1 && height_tiles >= 1) {
		if (width_tiles * height_tiles != tiles_read) {
			printf("Input file does not contain the specified dimensions! (Read %d tiles, dimensions are %dx%d = %d tiles)\n", tiles_read, width_tiles, height_tiles, width_tiles * height_tiles);
			return 1;
		}
	} else if (width_tiles >= 1 && height_tiles < 1) {
		if (tiles_read % width_tiles == 0) {
			height_tiles = tiles_read / width_tiles;
		} else {
			printf("Input file does not contain a whole number of rows of specified width! (Read %d tiles, and width is %d)\n", tiles_read, width_tiles);
			return 1;
		}
		if (verbose) {
			printf("Width specified, but no height. Using a height of %d/%d = %d tiles.\n", tiles_read, width_tiles, height_tiles);
		}
	} else if (width_tiles < 1&& height_tiles >= 1) {
		if (tiles_read % height_tiles == 0) {
			width_tiles = tiles_read / height_tiles;
		} else {
			printf("Input file does not contain a whole number of columns of specified height! (Read %d tiles, and height is %d)\n", tiles_read, height_tiles);
			return 1;
		}
		if (verbose) {
			printf("Height specified, but no width. Using a width of %d/%d = %d tiles.\n", tiles_read, height_tiles, width_tiles);
		}
	} else if (width_tiles < 1 && height_tiles < 1) {
		height_tiles = sqrt(tiles_read);
		while (tiles_read % height_tiles) {
			height_tiles--;
		}
		width_tiles = tiles_read / height_tiles;
		if (verbose) {
			printf("Neither width nor height specified. Using the most square layout of %dx%d.\n", width_tiles, height_tiles);
		}
	}

	if (in_file) {
		fclose(in_file);
	}

	if (tilemap_file) {
		fclose(tilemap_file);
	}

	if (verbose) {
		printf("Image dimensions: %dx%d (tiles) %dx%d (pixels)\n", width_tiles, height_tiles, width_tiles * TILE_WIDTH, height_tiles * TILE_HEIGHT);
	}

	redraw = 1;

	return 0;
}

void print_click_info(int x, int y) {
	static int last_x_tile = -1;
	static int last_y_tile = -1;
	int x_tile = x / (TILE_WIDTH * scale_factor);
	int y_tile = y / (TILE_HEIGHT * scale_factor);
	if (last_x_tile != x_tile || last_y_tile != y_tile) {
		int tile_id = y_tile * width_tiles + x_tile;
		if (using_tilemap) {
			tile_id = tile_map[tile_id];
		}
		printf("Tile coordinate (%d; %d) has tile number %d\n", x_tile, y_tile, tile_id);
	}
	last_x_tile = x_tile;
	last_y_tile = y_tile;
}

int main (int argc, char *argv[]) {
	SDL_Event event;

	arg0 = argv[0];

	// image height in tiles
	    height_tiles = -1;
	int height_specified = 0;
	// image width in tiles
	    width_tiles = -1;
	int width_specified = 0;
	// scaling
	    scale_factor = -1;
	int scale_specified = 0;

	/* error reporting */
	int errors = 0;

	for (int argn = 1; argn < argc; argn++) {
		if (argv[argn][0] == '-') {
			switch(argv[argn][1]) {
				case 'i':
					invert_palette();
					break;
				case '?':
					usage();
					break;
				case 'v':
					verbose = 1;
					break;
				case 'm':
					using_tilemap = 1;
					if (argv[argn][2] == '\0') {
						tilemap_filename = argv[++argn];
					} else {
						tilemap_filename = &(argv[argn][2]);
					}
					break;
				case 'h':
					height_specified = 1;
					if (argv[argn][2] == '\0') {
						height_tiles = atoi(argv[++argn]);
					} else {
						height_tiles = atoi(&(argv[argn][2]));
					}
					break;
				case 'w':
					width_specified = 1;
					if (argv[argn][2] == '\0') {
						width_tiles = atoi(argv[++argn]);
					} else {
						width_tiles = atoi(&(argv[argn][2]));
					}
					break;
				case 's':
					scale_specified = 1;
					if (argv[argn][2] == '\0') {
						scale_factor = atoi(argv[++argn]);
					} else {
						scale_factor = atoi(&(argv[argn][2]));
					}
					break;
				default:
					printf("Unrecognized option \'-%c\'!\n", argv[argn][1]);
					usage();
					break;
			}
		} else {
			in_filename = argv[argn];
		}
	}

	if (height_specified && height_tiles < 1) {
		printf("Invalid height %d specified.\n", height_tiles);
		errors++;
	}

	if (width_specified && width_tiles < 1) {
		printf("Invalid width %d specified.\n", width_tiles);
		errors++;
	}

	if (scale_specified && scale_factor < 1) {
		printf("Invalid scaling factor %d specified.\n", scale_factor);
		errors++;
	}

	if (!scale_specified) {
		if (verbose) {
			printf("No scaling specified (-s option), defaulting to %d.\n", DEFAULT_SCALING);
		}
		scale_factor = DEFAULT_SCALING;
	}

	errors += open_file();

	if (errors) {
		printf("%d errors encountered.\n", errors);
		usage();
	}

	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS);

	window = SDL_CreateWindow("gbview", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 0, 0, SDL_WINDOW_OPENGL);
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	SDL_EventState(SDL_DROPFILE, SDL_ENABLE);

	char *dropped_filename = NULL;

	while (running) {
		if (redraw) {
			if (verbose) {
				printf("Redrawing window.\n");
			}
			redraw = 0;
		}

		redraw_image();
		SDL_RenderPresent(renderer);

		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_KEYDOWN:
				keyDown(event.key.keysym.sym);
				break;
			case SDL_KEYUP:
				keyUp(event.key.keysym.sym);
				break;
			case SDL_QUIT:
				running = 0;
				break;
			case SDL_DROPFILE:
				if (dropped_filename) {
					SDL_free(dropped_filename);
					dropped_filename = NULL;
				}
				dropped_filename = event.drop.file;
				if (event.drop.type == SDL_DROPFILE) {
					using_tilemap = 0;
					width_tiles = 0;
					height_tiles = 0;
					in_filename = dropped_filename;
					tilemap_filename = NULL;
					if (verbose) {
						printf("Opening %s via drag and drop.\n", dropped_filename);
					}
					open_file();
				}
				break;
			case SDL_MOUSEBUTTONDOWN:
				if (event.button.button == SDL_BUTTON_LEFT) {
					print_click_info(event.button.x, event.button.y);
				}
				break;
			case SDL_MOUSEMOTION:
				if (event.motion.state & SDL_BUTTON_LMASK) {
					print_click_info(event.motion.x, event.motion.y);
				}
				break;
			}
		}
	}
	if (dropped_filename) {
		SDL_free(dropped_filename);
	}

	SDL_EventState(SDL_DROPFILE, SDL_DISABLE);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);

	SDL_Quit();
	return 0;
}
